--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.3 (Debian 11.3-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: User; Type: TABLE DATA; Schema: huijie; Owner: huijie
--

INSERT INTO huijie."User" (id, "loginName", password, "displayName", email, active, "createdAt", "updatedAt") VALUES (1, 'admin@huijie.com', '$argon2i$v=19$m=4096,t=3,p=1$716dMVUePKz7getGLC7aXA$e2WBmQkywAlvqjk/5mh9QS10ZLAs7pJ26nisixr9vrI', 'Admin', 'admin@huijie.com', true, '2019-11-04 18:46:56.029+08', '2019-11-04 18:46:57.222+08');


--
-- Name: User_id_seq; Type: SEQUENCE SET; Schema: huijie; Owner: huijie
--

SELECT pg_catalog.setval('huijie."User_id_seq"', 1, true);


--
-- PostgreSQL database dump complete
--
