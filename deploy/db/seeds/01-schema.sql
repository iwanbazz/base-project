--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.3 (Debian 11.3-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: huijie; Type: SCHEMA; Schema: -; Owner: huijie
--

CREATE SCHEMA huijie;


ALTER SCHEMA huijie OWNER TO huijie;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: User; Type: TABLE; Schema: huijie; Owner: huijie
--

CREATE TABLE huijie."User" (
    id integer NOT NULL,
    "loginName" character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    "displayName" character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE huijie."User" OWNER TO huijie;

--
-- Name: TABLE "User"; Type: COMMENT; Schema: huijie; Owner: huijie
--

COMMENT ON TABLE huijie."User" IS 'User stores all user information';


--
-- Name: User_id_seq; Type: SEQUENCE; Schema: huijie; Owner: huijie
--

CREATE SEQUENCE huijie."User_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE huijie."User_id_seq" OWNER TO huijie;

--
-- Name: User_id_seq; Type: SEQUENCE OWNED BY; Schema: huijie; Owner: huijie
--

ALTER SEQUENCE huijie."User_id_seq" OWNED BY huijie."User".id;


--
-- Name: User id; Type: DEFAULT; Schema: huijie; Owner: huijie
--

ALTER TABLE ONLY huijie."User" ALTER COLUMN id SET DEFAULT nextval('huijie."User_id_seq"'::regclass);


--
-- Name: User User_loginName_key; Type: CONSTRAINT; Schema: huijie; Owner: huijie
--

ALTER TABLE ONLY huijie."User"
    ADD CONSTRAINT "User_loginName_key" UNIQUE ("loginName");


--
-- Name: User User_pkey; Type: CONSTRAINT; Schema: huijie; Owner: huijie
--

ALTER TABLE ONLY huijie."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--
