import { models as Models } from '../../config/database';

export const getUserModel = () => {
  return Models.User;
};
