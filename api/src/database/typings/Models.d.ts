import User from '../models/User';
import ModelBase from '../models/ModelBase';

export interface Models {
  User: typeof User;
  [key: string]: typeof ModelBase;
}
