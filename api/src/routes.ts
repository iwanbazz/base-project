import express from 'express';

import AuthController from './controllers/AuthController';
import PasswordController from './controllers/PasswordController';
import UserController from './controllers/UserController';
import HealthCheckController from './controllers/HealthCheckController';

const router = express.Router();

router.use('/', AuthController);
router.use('/', PasswordController);
router.use('/healthcheck', HealthCheckController);
router.use('/users', UserController);

export default router;
