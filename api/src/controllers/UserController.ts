import express, { RequestHandler } from 'express';
import { OK } from 'http-status-codes';

import Logger from '../Logger';
import { Authentication } from '../config/passport';
import * as UserDao from '../database/dao/UserDao';

const UserController = express.Router();
const LOG = new Logger('UserController.ts');

const getCurrentUserHandler: RequestHandler = async (req, res, next) => {
  try {
    const { id } = req.user;

    const currentUser = await UserDao.getById(id);

    return res.status(OK).json({
      displayName: currentUser.get('displayName')
    });
  } catch (err) {
    LOG.error(err);
    return next(err);
  }
};

UserController.get('/current', Authentication.AUTHENTICATED, getCurrentUserHandler);

export default UserController;
