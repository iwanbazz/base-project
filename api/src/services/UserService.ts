import Logger from '../Logger';
import * as UserDao from '../database/dao/UserDao';

const LOG = new Logger('UserService.ts');

export const getUserById = async (userId: number) => {
  return await UserDao.getById(userId);
};
